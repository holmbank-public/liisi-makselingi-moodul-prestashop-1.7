<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Liisibanklink
 *  @copyright 2019 Veebipoed.ee, Liisibanklink
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

// File: /upgrade/upgrade-2.1.0

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1_0($module)
{
    // Script examples:
    // ---------------------------
    // changing DATABASE structure
    // ---------------------------
    //  $sql = 'ALTER TABLE '._DB_PREFIX_.'module_order ADD method VARCHAR(255), ADD payment_tool VARCHAR(255)';
    //if (!Db::getInstance()->execute($sql)) {
    //    return false;
    // }
    // ----------------------------------------
    // Adding/updating hooks between versions
    // ----------------------------------------
    //if (!$module->registerHook('header')
    //   || !$module->registerHook('actionObjectCurrencyAddAfter')) {
    //   return false;
    // }
    // ----------------------------------------
    // Adding/updating configuration data
    // ----------------------------------------
    //    if (!Configuration::updateValue('MODULE_BRAINTREE_ENABLED', 0)
    //        || !Configuration::updateValue('MODULE_CRON_TIME', date('Y-m-d H:m:s'))
    //        || !Configuration::updateValue('MODULE_BY_BRAINTREE', 0)) {
    //        return false;
    //    }
    // ----------------------------------------
    // Adding/updating install function
    // ----------------------------------------
    //    if (!$module->installOrderState()) {
    //    return false;
    //     }

    return true;
}
