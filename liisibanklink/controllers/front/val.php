<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Liisibanklink
 *  @copyright 2019 Veebipoed.ee, Liisibanklink
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('Veebipoed_ValModuleFrontController')) {
    include(_PS_MODULE_DIR_.'liisibanklink/libs/veebipoed_val_module_controller.php');
}

class LiisibanklinkValModuleFrontController extends Veebipoed_ValModuleFrontController
{

    /**
     * [initContent description]
     * @return [type] [description]
     */
    public function initContent()
    {
        parent::initContent();

        $verification = $this->vertifySign('VK_MAC');
        $order = new Order(Tools::getValue('VK_STAMP', null));

        if ($order->id && $order->current_state == Configuration::get('PS_OS_PAYMENT')) {
            if (Tools::getValue('VK_AUTO', 'Y') == 'N') {
                Tools::redirectLink($this->getOrderConfUrl($order));
            }
            $this->context->smarty->assign(array(
                'banklink_msg' => $this->module->l('the order has already been paid', 'val'),
                'msg_class' => 'info'
            ));
            $this->setTemplate('module:liisibanklink/views/templates/front/final.tpl');
        } elseif ($order->id && $verification == 0) {
            $this->setError($order, $this->module->l('Invalid public key', 'val'));
        } elseif ($order->id && $verification == 1) {
            if (Tools::getValue('VK_SERVICE') == '1111') {
                $this->validate($order);
            } elseif (Tools::getValue('VK_SERVICE') == '1911') {
                $this->setCanceled($order, $this->module->l('Order canceled', 'val'));
            } else {
                $this->setError($order, $this->module->l('Problems with liisi server', 'val'));
            }
        } else {
            Tools::redirectLink(__PS_BASE_URI__);
        }
    }

    /**
     * [getResponseFields description]
     * @return [type] [description]
     */
    public function getResponseFields()
    {
        if (Tools::getIsset('VK_SERVICE')) {
            $array = array(
                'VK_SERVICE' => Tools::getValue('VK_SERVICE'),
                'VK_VERSION' => Tools::getValue('VK_VERSION'),
                'VK_SND_ID' => Tools::getValue('VK_SND_ID'),
                'VK_REC_ID' => Tools::getValue('VK_REC_ID'),
                'VK_STAMP' => Tools::getValue('VK_STAMP')
            );

            switch (Tools::getValue('VK_SERVICE')) {
                case '1111':
                    $array['VK_T_NO'] = Tools::getValue('VK_T_NO');
                    $array['VK_AMOUNT'] = Tools::getValue('VK_AMOUNT');
                    $array['VK_CURR'] = Tools::getValue('VK_CURR');
                    $array['VK_REC_ACC'] = Tools::getValue('VK_REC_ACC');
                    $array['VK_REC_NAME'] = Tools::getValue('VK_REC_NAME');
                    $array['VK_SND_ACC'] = Tools::getValue('VK_SND_ACC');
                    $array['VK_SND_NAME'] = Tools::getValue('VK_SND_NAME');
                    $array['VK_REF'] = Tools::getValue('VK_REF');
                    $array['VK_MSG'] = Tools::getValue('VK_MSG');
                    $array['VK_T_DATETIME'] = Tools::getValue('VK_T_DATETIME');
                    break;
                case '1911':
                    $array['VK_REF'] = Tools::getValue('VK_REF');
                    $array['VK_MSG'] = Tools::getValue('VK_MSG');
                    break;
            }
        }

        $data = '';
        foreach ($array as $label) {
            $data .= $this->module->padding($label) . $label;
        }
        return $data;
    }

    /**
     * [validate description]
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    public function validate($order)
    {
        $total_paid_real = Tools::getValue('VK_AMOUNT');

        if ($total_paid_real == $order->total_paid) {
            $this->setValid($order);
        } else {
            $this->setError($order, $this->module->l('the amount charged and the amount paid does not match', 'val'));
        }
    }
}
