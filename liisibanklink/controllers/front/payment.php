<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Liisibanklink
 *  @copyright 2019 Veebipoed.ee, Liisibanklink
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('Veebipoed_PaymentModuleFrontController')) {
    include(_PS_MODULE_DIR_.'liisibanklink/libs/veebipoed_payment_module_controller.php');
}
  
class LiisibanklinkPaymentModuleFrontController extends Veebipoed_PaymentModuleFrontController
{
    
}
