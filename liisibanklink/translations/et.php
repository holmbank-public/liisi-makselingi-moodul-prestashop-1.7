<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{liisibanklink}prestashop>liisibanklink_68fa7f9e472409c8780506176136b799'] = 'Liisi pangalingi makse';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_04d72204ebf60d0a238a23b7afb9e5a7'] = 'moodul Liisi pangalingi makseteks';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_f990493af3321939ca512f8f2cace108'] = 'Maksta %s';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Lubatud';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_b9f5c797ebbf55adccdd8539a65a0241'] = 'Keelatud';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_49badd98c7c0ce34a83f8702b101423d'] = 'Test server';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_4489147fd9476de5f43bd8d9273b12fe'] = 'Live server';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_9aa1b03934893d7134a660af4204f2a9'] = 'Server';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_ed6445336472aef39084720adcf903b9'] = 'Avalik võti';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_6eb9040e470e8018db394832a528f56a'] = 'Privaatne võti';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_c335ba8bf137c0d8a192f9eadae46019'] = 'Privaatse võtme parool';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_3aa8778f62eabcc99108672ad03a32f1'] = 'Makse kinnitus tekst';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_baf0cfd8a89d881eee72210d7008884e'] = 'Jäta kinnitus vahele';
$_MODULE['<{liisibanklink}prestashop>liisibanklink_733154f7eeee6cd45162e766e22e47ee'] = 'Tellimus';
$_MODULE['<{liisibanklink}prestashop>veebipoed_paymentmodule_f990493af3321939ca512f8f2cace108'] = 'Maksta %s';
$_MODULE['<{liisibanklink}prestashop>confirmation_6c89fb24f68448e8da755b09d6d0adea'] = 'Teie ost on kinnitatud!';
$_MODULE['<{liisibanklink}prestashop>confirmation_f04eccb70670892c5cb0aaf940677d16'] = 'Palun vajutage allpool olevat nuppu kui teid ei ole suunatud 3 sekundi jooksul';
$_MODULE['<{liisibanklink}prestashop>confirmation_695af09a30edbfdeef3decb030d88e7f'] = 'Alusta maksmist';
$_MODULE['<{liisibanklink}prestashop>confirmation_0878a2bc8337f130f4fd43ec7af8606a'] = 'Tekkis viga teie makse protsessimisel';
$_MODULE['<{liisibanklink}prestashop>option_info_ajax_99938b17c91170dfb0c2f3f8bc9f2a85'] = 'Maksta';
