/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Liisibanklink
 *  @copyright 2019 Veebipoed.ee, Liisibanklink
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

var paymentIsClicked = false;
$( document ).ready(function() {
      console.log('Script starts (liisi)');
      result = checkConditionLiisi();
      
    $('#conditions-to-approve').on('click', function(e){
        result = checkConditionLiisi();
        console.log('Terms and conditions is accepted (L): '+result); 
    });
    
    $('.ps-shown-by-js').on('click', function(e){
        result = checkConditionLiisi();
        console.log('terms and conditions is accepted (L): '+result); 
    });
         });

function checkConditionLiisi(){ // terms-and-conditions
    console.log('Check Condition (L)... ');
     var a = $('input[id="conditions_to_approve[terms-and-conditions]"]').prop('checked');
     var $payBotton = $('#liisibanklink_banklink_payment_botton');
       if (a){b = false;} else {b =  true;}
        $payBotton.prop('disabled', b);
        return b;    
}

function doAjax(evt, name)
{
    console.log('-- do Ajax starts -- '+paymentIsClicked);
    console.log(evt, name);
   if (evt.stopPropagation) {
      evt.stopPropagation();
   } else {
      window.event.cancelBubble = true;
   }

   if (paymentIsClicked) {
      return false;
   }

   $('body').append('<div class="ajax_loader"></div>');

   paymentIsClicked = true;
   $.ajax({
      type: 'POST',
      url: $('#'+name+'_ajax').val(),
      dataType: 'json',
      success: function(json) {
         if (json.success) {
            inputs = [];
            for (var key in json.data) {
               if (json.data.hasOwnProperty(key)) {
                  inputs.push('<input type="hidden" name="'+key+'" value=\''+json.data[key]+'\'/>');
               }
            }
            $('form#'+name).append(inputs.join('')).submit();
         }
      }
   });
}
