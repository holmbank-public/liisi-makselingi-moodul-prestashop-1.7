{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2019 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}
{if $status == 'ok'}
	<p class="alert alert-success">{l s='Payment successfully processed' mod='liisibanklink'}</p>
	<div class="box">
		<p>{l s='Order tracking' mod='liisibanklink'} <a href="{$linkToOrder}">{l s='here.' mod='liisibanklink'}</a></p>
	</div>
{else}
	<p class="alert alert-danger">{l s='Payment failed' mod='liisibanklink'}</p>
{/if}