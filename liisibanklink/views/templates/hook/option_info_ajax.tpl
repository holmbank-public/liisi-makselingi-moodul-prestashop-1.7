{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2019 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}
<section class="{$moduleName}_add_section">
	<div class="liisibanklink_logo_block"></div>
	{if isset($ajax) && $ajax}
             <div class="ps-shown-by-js">
		<button id="{$moduleName}_banklink_payment_botton" class="veebipoed_banklink btn btn-outline-success js-payment-{$moduleName}" href="{$href}" onclick="doAjax(event, '{$moduleName}');return false;">{l s='Pay' mod='liisibanklink'}</button>
                </div>
	{else}
		<button class="veebipoed_banklink btn btn-outline-success js-payment-{$moduleName}" href="#" onclick="doAjax(event, '{$moduleName}');return false;">{l s='Pay' mod='liisibanklink'}</button>
	{/if}
	{if isset($ajax) && $ajax}
		<input type="hidden" name="{$moduleName}_ajax" id="{$moduleName}_ajax" value="{$ajax_url}">
		<form id="{$moduleName}" name="{$moduleName}" method="POST" target="_self" action="{$href}">
		</form>
	{/if}
</section>