{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2019 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}
{extends "$layout"}
{block name="content"}

{capture name=path}
    {$displayName}
{/capture}
<div class="first_confirm_page">
    <div class="box">
        <h3 class="page-subheading">{l s='Liisi credit' mod='liisibanklink'}</h3>
        <p>{l s='You have selected Liisi payment' mod='liisibanklink'} . {l s='If you want to proceed, please click "Confirm", otherwise click "Back"' mod='liisibanklink'}</p>
    </div>
    <div class="cart_navigation clearfix" id="cart_navigation">
        <a class="back_button btn btn-outline-secondary" href="{$back_href}">{l s='Back' mod='liisibanklink'}</a>
        {if isset($ajax) && $ajax}
            <a class="btn btn-outline-success" href="#" onclick="doAjax(event, '{$moduleName}');return false;">{l s='Confirm' mod='liisibanklink'}</a>
        {else}
            <a class="btn btn-outline-success" href="{$href}">{l s='Confirm' mod='liisibanklink'}</a>
        {/if}
        {* <a class="btn btn-outline-success" href="{if isset($ajax) && $ajax}#{else}{$href}{/if}" {if isset($ajax) && $ajax}onclick="doAjax(event, '{$moduleName}');return false;"{/if}>{l s='Confirm' mod='liisibanklink'}
        </a> *}
    </div>
    {if isset($ajax) && $ajax}
        <input type="hidden" name="{$moduleName}_ajax" id="{$moduleName}_ajax" value="{$ajax_url}">
        <form id="{$moduleName}" name="{$moduleName}" method="POST" target="_self" action="{$href}">
        </form>
    {/if}
</div>    
{/block}