{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2019 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}
{extends "$layout"}
{block name="content"}
<div class="purchase_confirmation">
    {if $status == 'ok'}
    {literal}
        <script>
            window.onload = function() { 
                document.getElementById("liisibanklink").submit(); 
                }
        </script>
    {/literal}

    <p class="alert alert-success">{l s='Your Purchase is confirmed!' mod='liisibanklink'}</p>
    <div class="box">
        <p>{$banklink_msg}</p>
        <p>{l s='Please click button below if you are not redirected within a 3 seconds' mod='liisibanklink'}<p>
    </div>
    <p>
        <form name='{$moduleName}' id='{$moduleName}' action='{$bank_url}' target='_self' method='POST'>
            {foreach from=$formdata key=name item=value}
                <input type="hidden" name="{$name}" value="{$value}"/>
            {/foreach}
            <p class="cart_navigation clearfix" id="cart_navigation">
                <button class="start_payment btn btn-outline-success" type='submit'>{l s='Start a payment' mod='liisibanklink'}</button>
            </p>
        </form>
    </p>
    {else}
        <p class="alert alert-danger">{l s='There has been an error processing your payment' mod='liisibanklink'}</p>
    {/if}
</div>
{/block}