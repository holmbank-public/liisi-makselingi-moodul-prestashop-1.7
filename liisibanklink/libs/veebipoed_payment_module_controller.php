<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2018 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Veebipoed_PaymentModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	
	public function initContent() {

		parent::initContent();

		if(Configuration::get($this->module->prefixed('ajax_conf')))
		{
			$this->context->controller->addCSS($this->module->getPathUri() . 'views/css/banklink.css');
			$this->context->smarty->assign(array(
				'ajax' => true,
				'ajax_url' => $this->context->link->getModuleLink($this->module->name, 'ajax', array(), true)
			));
			$server = Configuration::get($this->module->prefixed('server'));
			$this->addJS($this->module->getPathUri().'views/js/ajax.js');
			$href = $this->module->getConfig($server, 'url');
		}
		else
		{
			$href = $this->context->link->getModuleLink($this->module->name, 'confirmation', array(), true);
		}

		$this->context->smarty->assign(array(
			'href' => $href,
			'back_href' => $this->context->link->getPageLink('order.php', true),
			'moduleName' => $this->module->name,
			'displayName' => $this->module->displayName
		));

		$this->setTemplate('module:liisibanklink/views/templates/front/payment_execution.tpl');
	}
}
