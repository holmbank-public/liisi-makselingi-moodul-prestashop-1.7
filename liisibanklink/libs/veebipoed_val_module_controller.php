<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2018 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

abstract class Veebipoed_ValModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	
	/**
	 * [getResponseFields description]
	 * @return [type] [description]
	 */
	abstract public function getResponseFields();

	/**
	 * Set order state to error and set error msg
	 * @param order  $order Order object
	 * @param string $msg   Error message
	 */
	public function setError($order, $msg)
	{
		$order->setCurrentState(Configuration::get('PS_OS_ERROR'));
		$this->context->smarty->assign(array(
			'banklink_msg' => $msg,
			'msg_class' => 'danger'
		));
		$this->setTemplate('module:liisibanklink/views/templates/front/final.tpl');
	}

	public function setCanceled($order, $msg)
	{
		$order->setCurrentState(Configuration::get('PS_OS_CANCELED'));
		$this->context->smarty->assign(array(
			'banklink_msg' => $msg,
			'msg_class' => 'info'
		));
		$this->setTemplate('module:liisibanklink/views/templates/front/final.tpl');
	}

	/**
	 * Get order confirmation url
	 * @param  order $order Order object
	 * @return string       Confirmation url
	 */
	public function getOrderConfUrl($order)
	{
		return __PS_BASE_URI__ .
			'order-confirmation.php?id_cart='.$order->id_cart.
			'&id_module='. $this->module->id.
			'&id_order='.$order->id.
			'&key='.$order->secure_key;
	}

	/**
	 * Set order to valid
	 * @param Order $order Order object
	 */
	public function setValid($order)
	{
		$order->valid = true;
		$order->setCurrentState(Configuration::get('PS_OS_PAYMENT'));
		Tools::redirectLink($this->getOrderConfUrl($order));
	}

	/**
	 * Verify sign
	 * @return int 1 if corrent, 0 incorrent, otherwise error
	 */
	public function vertifySign($key) 
	{		
		$server = Configuration::get($this->module->prefixed('server'));
		
		if (Tools::getIsset($key)) {
			$signature = base64_decode(Tools::getValue($key));	
		} else {
			return 2;
		}

		$data = $this->getResponseFields();
		$cert = $this->module->getConfig($server, 'public_key');
		$publicKey = openssl_get_publickey($cert);
		$out = openssl_verify($data, $signature, $publicKey);
		openssl_free_key($publicKey);
		return $out;
	}
}
