<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Liisibanklink
*  @copyright 2018 Veebipoed.ee, Liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Veebipoed_ConfirmationModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	
	public function initContent()
	{	
		parent::initContent();

		$server = Configuration::get($this->module->prefixed('server'));

		$order = $this->module->confirmOrder();
		
		if ($order->getCurrentState() == Configuration::get($this->module->getStatusName())) {
		 	$status = 'ok';
		} else {
			$status = 'error';
		}
	
		$this->context->smarty->assign(array(
			'formdata' => $this->module->getFormFields($order, $server),
			'status' => 'ok',
			'banklink_msg' => trim(Configuration::get(
				$this->module->prefixed('payment_confirm'),
				$this->context->language->id)
			),
			'bank_url' => $this->module->getConfig($server, 'url'),
			'moduleName' => $this->module->name
		));
		$this->setTemplate('module:liisibanklink/views/templates/front/confirmation.tpl');
	}
}
