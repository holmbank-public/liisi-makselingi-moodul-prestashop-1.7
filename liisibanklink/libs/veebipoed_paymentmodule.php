<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, liisibanklink
*  @copyright 2019 Veebipoed.ee, liisibanklink
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

abstract class Veebipoed_Paymentmodule extends PaymentModule
{
    /**
     * Module config keys
     * @return array config keys
     */
    abstract public function getParams();

    /**
     * Multilang config keys
     * @return array Multilang config keys
     */
    abstract public function getLangParams();

    /**
     * Module config form
     * @return array Form config
     */
    abstract public function getFields();

    /**
     * Module status name, used for saving status id to database
     * @return string status name
     */
    abstract public function getStatusName();

    /**
     * Mail variables getter
     * @return array Mail variables
     */
    public function getMailVars()
    {
        return array(
            '{bankwire_owner}' => Configuration::get('BANK_WIRE_OWNER'),
            '{bankwire_details}' => nl2br(Configuration::get('BANK_WIRE_DETAILS')),
            '{bankwire_address}' => nl2br(Configuration::get('BANK_WIRE_ADDRESS'))
        );
    }

    /**
     * Form fields for generating submit form(making payment)
     * @param Order $order Order object
     * @param int $server Current server(live/test)
     * @return [type] [description]
     */
    abstract public function getFormFields($order, $server);

    /**
     * install
     * @return true true, if successful
     */
    public function install()
    {
        if (!parent::install() or
        !$this->registerHook('displayInvoice') or
        !$this->registerHook('paymentOptions') or
        !$this->registerHook('paymentReturn') or
        !$this->registerHook('header') or
        !$this->createOrderState() or
        !$this->createVariables()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * uninstall
     * @return boolean true, if successful
     */
    public function uninstall()
    {
        if (!parent::uninstall() or
        !$this->unregisterHook('displayInvoice') or
        !$this->unregisterHook('paymentOptions') or
        !$this->unregisterHook('paymentReturn') or
        !$this->unregisterHook('header')
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * get config variables
     * @param int $server Live or test server
     * @param string $key key
     * @return Mixed Config parameter
     */
    public function getConfig($server = null, $key = null)
    {
        if (!is_null($server) && !empty($key)) {
            return $this->config[$server][$key]; 
        }
        
        elseif (!is_null($server)) {
            return $this->config[$server];
        } else {
            return $this->config;
        }
    }

    /**
     * Get module file path
     * @return string file path
     */
    public function getFileName()
    {
        return $this->getLocalPath().$this->name.'.php';
    }

    /**
     * padding function
     * @param  string $string string
     * @return [type]         padded string
     */
    public function padding($string)
    {
        if ($this->getEncoding()) {
            return str_pad(mb_strlen($string, $this->getEncoding()), 3, '0', STR_PAD_LEFT);
        } else {
            return str_pad(strlen($string), 3, '0', STR_PAD_LEFT);
        }
    }

    /**
     * Encoding
     * @return Mixed if encoding is set, then encoding
     */
    public function getEncoding()
    {
        return false;
    }

    /**
     * Paymentreturn hook
     * @param array $params params
     * @return string html
     */
    public function hookPaymentReturn($params)
    {
        $order = $params['order'];
        $state = $order->getCurrentState();
                
        if ($state == _PS_OS_PAYMENT_) {
            $status = 'ok';
        } else {
            $status = 'error';
        }

        $this->smarty->assign(array(
            'status' => $status,
            'linkToOrder' => $this->getOrderLink($order)
        ));
        
        return $this->fetch($this->getFullPathToTpl('views/templates/hook/paymentReturn.tpl'));
    }
    
    public function getFullPathToTpl($param)
    {
        $result = 'module:'.$this->name.'/'.$param;
        return $result;
    }
    
    /**
     * Invoice hook
     * @param  Array $params params
     * @return nothing
     */
    public function hookDisplayInvoice($params)
    {
    }

    /**
     * Module configuration page
     * @return string confuration page html
     */
    public function getContent()
    {
        $html = '';
        if (Tools::isSubmit('submit'.$this->name)) {
            foreach ($this->getParams() as $param) {
                Configuration::updateValue($this->prefixed($param), Tools::getValue($param));
            }

            foreach ($this->getLangParams() as $param) {
                $langvalues = array();
                foreach (Language::getLanguages(false) as $key => $language) {
                    $langvalues[$language['id_lang']] = Tools::getValue($param.'_'.$language['id_lang']);
                }
                Configuration::updateValue($this->prefixed($param), $langvalues, true);
            }
            $html .= $this->displayConfirmation($this->l('Settings updated'));
        }

        $html .= $this->_displayInfo();
        $html .= $this->_showForm();

        return $html;
    }

    private function _displayInfo()
    {
        return $this->display($this->getFileName(), 'bo_info.tpl');
    }

    /**
     * Form generation
     * @return string form html
     */
    private function _showForm()
    {
        $field_values = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper = new HelperForm();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => $this->getFields(),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->languages = Language::getLanguages(false);
        foreach ($helper->languages as $k => $language) {
            $helper->languages[$k]['is_default'] = (int)($language['id_lang'] == $helper->default_form_language);
        }

        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        foreach ($this->getParams() as $param) {
            $field_values[$param] = Configuration::get($this->prefixed($param));
        }

        foreach ($this->getLangParams() as $param) {
            foreach ($helper->languages as $key => $language) {
                $field_values[$param][$language['id_lang']] =
                    Configuration::get($this->prefixed('payment_confirm'), $language['id_lang']);
            }
        }

        $helper->fields_value = $field_values;

        return $helper->generateForm($fields_form);
    }

    /**
     * Payment hook
     * @param  Array $params params
     * @return string payment template
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        $server = Configuration::get($this->prefixed('server'));
        $skip_confirm = Configuration::get($this->prefixed('skip_confirm'));
        $ajax_confirm = Configuration::get($this->prefixed('ajax_conf'));
         if (isset($this->frontModuleText)) {
            $moduleText = $this->frontModuleText;
        } else {
            $moduleText = sprintf($this->l('Pay by %s'), $this->displayName);// name of Payment in FRONT
        }
        $PayOption = new PaymentOption();
        $PayOption->setCallToActionText($moduleText);
                
        if ($skip_confirm) {
            if ($ajax_confirm) {
                //skip confirmation ON and Ajax ON
                $href = $this->config[$server]['url'];
                $this->context->smarty->assign(array(
                    'ajax' => true,
                    'ajax_url' => $this->context->link->getModuleLink($this->name, 'ajax', array(), true),
                    'moduleName' => $this->name,
                    'href' => $href
                ));
                $PayOption
                    ->setBinary(true)
                    ->setAdditionalInformation($this->context->smarty->fetch($this->getFullPathToTpl('views/templates/hook/option_info_ajax.tpl')))
                    ;
                return [$PayOption];
            } else {
                //skip confirmation ON and Ajax OFF
                $PayOption
                    ->setAction($this->context->link->getModuleLink($this->name, 'confirmation', array(), true))
                    ->setAdditionalInformation($this->context->smarty->fetch($this->getFullPathToTpl('views/templates/hook/option_info.tpl')))
                    ;
                return [$PayOption];
            }
        }
        //confirmation OFF and (ajax ON or OFF)
        $PayOption
                ->setAction($this->context->link->getModuleLink($this->name, 'payment', array(), true))
                ->setAdditionalInformation($this->context->smarty->fetch($this->getFullPathToTpl('views/templates/hook/option_info.tpl')))
                ;
        return [$PayOption];
    }

    /**
     * Header hook (for js/css)
     * @param  Array $params params
     * @return nothing
     */
    public function hookHeader($params)
    {
        $skip_confirm = Configuration::get($this->prefixed('skip_confirm'));
        $ajax_confirm = Configuration::get($this->prefixed('ajax_conf'));
 
        $this->context->controller->addJs(($this->_path."views/js/").'ajax.js', 'all');
        $this->context->controller->addCSS(($this->_path."views/css/").'banklink.css', 'all');
    }
    
    /**
     * Create config variabled
     * @return boolean true if successful
     */
    public function createVariables()
    {
        if (Configuration::updateValue($this->prefixed('ajax_conf'), 1) === false ||
            Configuration::updateValue($this->prefixed('server'), 0) === false ||
            Configuration::updateValue($this->prefixed('skip_confirm'), 1) === false
        ) {
            return false;
        }
        return true;
    }

    /**
     * Validate order
     * @return Order Current order object
     */
    public function confirmOrder()
    {
        $server = Configuration::get($this->prefixed('server'));
        $mailVars = $this->getMailVars($server);

        $this->validateOrder(
            $this->context->cart->id,
            Configuration::get($this->getStatusName()),
            $this->context->cart->getOrderTotal(),
            $this->displayName,
            null,
            $mailVars,
            $this->context->cart->id_currency,
            false,
            $this->context->customer->secure_key
        );

        return new Order($this->currentOrder);
    }
    
    /**
     * Create Order State, coping mails and logo
     * @return boolean true, if successful
     */
    public function createOrderState()
    {
        $orderStateExist = false;
        $mailCopied = false;
        $logoCopied = false;
        $orderStateId = Configuration::get($this->getStatusName());
        $description = sprintf('Awaiting %s', $this->displayName);
        if ($orderStateId) {
            $orderState = new OrderState($orderStateId);
            if ($orderState->id && !$orderState->deleted) {
                $orderStateExist = true;
            }
        } else {
            $query = 'SELECT os.`id_order_state` '.
            'FROM `%1$sorder_state_lang` osl '.
            'LEFT JOIN `%1$sorder_state` os '.
            'ON osl.`id_order_state`=os.`id_order_state` '.
            'WHERE osl.`name`="%2$s" AND os.`deleted`=0';
            $orderStateId =  Db::getInstance()->getValue(sprintf($query, _DB_PREFIX_, $description));
            if ($orderStateId) {
                Configuration::updateValue($this->getStatusName(), $orderStateId);
                $orderStateExist = true;
            }
        }
            
        if (!$orderStateExist) {
            $languages = Language::getLanguages(false);
            $orderState = new OrderState();
            foreach ($languages as $lang) {
                $orderState->name[$lang['id_lang']] = $description;
            }
            $orderState->send_email = 1;
            $orderState->invoice = 1;
            $orderState->color = "lightblue";
            $orderState->unremovable = 0;
            $orderState->logable = 0;
            $orderState->delivery = 0;
            $orderState->hidden = 0;
            $orderState->template = 'bankwire';
            if ($orderState->add()) {
                Configuration::updateValue($this->getStatusName(), $orderState->id);
                $orderStateExist = true;
            }
        }

        $file = $this->getLocalPath().'logo.png';
        $newfile = _PS_IMG_DIR_.'os/' . $orderState->id . '.png';

        $logoCopied = (is_file($newfile) || (!is_file($newfile) && copy($file, $newfile)));
            
        return ($orderStateExist);
    }

    /**
     * Prefixed keys
     * @param  string $key key
     * @return string      prefixed key
     */
    public function prefixed($key)
    {
        return $this->name.'_'.$key;
    }

    /**
     * Get order tracking url for customer
     * @param Order $order Order object
     * @return string tracking url
     */
    public function getOrderLink($order)
    {
        $customer = new Customer($order->id_customer);
        
        if ($customer->isGuest()) {
            $orderLink = $this->context->link->getPageLink(
                'guest-tracking',
                null,
                null,
                'id_order='.$order->reference.'&email='.$customer->email.'&submitGuestTracking=1'
            );
        } else {
            $orderLink = $this->context->link->getPageLink('history');
        }
        return $orderLink;
    }

/**
* Generating MAC code
* @param Array $source data for MAC calculating
* @param int $server Test or live server
* @return string MAC code
*/
    public function generateMAC(Array $source, $server)
    {
        $data = '';
        foreach ($source as $label) {
            $data .= $this->padding($label) . $label;
        }
        $privKey = openssl_get_privatekey(
            $this->getConfig($server, 'private_key'),
            $this->getConfig($server, 'private_key_pass')
        );
        if (!$privKey) {
            echo sprintf(
                $this->l('There is an error with %s Modules private key. Please contact the servers administrator.'),
                $this->displayName
            );
            return 0;
        }
        openssl_sign($data, $signature, $privKey);
        $VK_MAC = base64_encode($signature);
        openssl_free_key($privKey);
        return $VK_MAC;
    }
}
