<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Liisibanklink
 *  @copyright 2019 Veebipoed.ee, Liisibanklink
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('Veebipoed_Paymentmodule')) {
    include(_PS_MODULE_DIR_ . 'liisibanklink/libs/veebipoed_paymentmodule.php');
}

class Liisibanklink extends Veebipoed_Paymentmodule
{

    const TEST = 0; // TEST = 0 - config[0]
    const LIVE = 1; // TEST = 1 - config[1]

    public function __construct()
    {
        $this->name = 'liisibanklink';
        $this->tab = 'payments_gateways';
        $this->version = '2.1.0';
        $this->author = 'Veebipoed.ee';
        $this->bootstrap = true;
        parent::__construct();
        
        $this->displayName = $this->l('Liisi banklink payment');
        $this->description = $this->l('module for making Liisi banklink payments');
        $this->frontModuleText = sprintf($this->l('Pay by %s'), $this->displayName);
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->is_eu_compatible = 1;
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
        //$this->module_key = 'cc369cefc3edda5f2ff838e30bb1a339';

        $this->config = array();
        // TEST = 0 - config[0]
        $this->config[] = array(
            'url' => 'https://prelive.liisi.ee/api/ipizza/',
            'vk_snd' => Configuration::get('liisibanklink_SNDID'),
            'vk_ref' => '',
            'vk_acc' => '',
            'vk_name' => '',
            'vk_lang' => 'EST',
            'vk_service' => '1012',
            'vk_version' => '008',
            'vk_charset' => 'UTF-8',
            'private_key_pass' => Configuration::get('liisibanklink_private_key_pass'),
            'private_key' => Configuration::get('liisibanklink_private_key'),
            'public_key' => Configuration::get('liisibanklink_public_key')
        );
        // TEST = 1 - config[1]
        $this->config[] = array(
            'url' => 'https://klient.liisi.ee/api/ipizza/',
            'vk_snd' => Configuration::get('liisibanklink_SNDID'),
            'vk_ref' => '',
            'vk_acc' => '', // if _VK_SERVEICE_ 1001 is chosen
            'vk_name' => '', // if _VK_SERVEICE_ 1001 is chosen
            'vk_lang' => 'EST',
            'vk_service' => '1012',
            'vk_version' => '008',
            'vk_charset' => 'UTF-8',
            'private_key_pass' => Configuration::get('liisibanklink_private_key_pass'),
            'private_key' => Configuration::get('liisibanklink_private_key'),
            'public_key' => Configuration::get('liisibanklink_public_key')
        );
    }

    public function getParams()
    {
        return array('server', 'ajax_conf', 'skip_confirm', 'private_key', 'public_key', 'private_key_pass', 'SNDID');
    }

    public function getLangParams()
    {
        return array('payment_confirm');
    }

    public function getFields()
    {
        $boolean = array(
            array(
                'id' => 'active_on',
                'value' => 1,
                'label' => $this->l('Enabled')
            ),
            array(
                'id' => 'active_off',
                'value' => 0,
                'label' => $this->l('Disabled')
            )
        );

        $options = array(
            array('id' => self::TEST, 'name' => $this->l('Test server')),
            array('id' => self::LIVE, 'name' => $this->l('Live server')),
        );

        return array(
            array(
                'type' => 'select',
                'label' => $this->l('Server'),
                'name' => 'server',
                'required' => true,
                'lang' => false,
                'options' => array(
                    'query' => $options,
                    'id' => 'id',
                    'name' => 'name'
                )
            ),
            array(
                'type' => 'text',
                'label' => $this->l('SND ID'),
                'name' => 'SNDID',
                'required' => true,
                'lang' => false
            ),
            array(
                'type' => 'textarea',
                'rows' => 10,
                'cols' => 100,
                'label' => $this->l('Public key'),
                'name' => 'public_key',
                'required' => true,
                'lang' => false
            ),
            array(
                'type' => 'textarea',
                'rows' => 10,
                'cols' => 100,
                'label' => $this->l('Private key'),
                'name' => 'private_key',
                'required' => true,
                'lang' => false
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Private key password'),
                'name' => 'private_key_pass',
                'required' => false,
                'lang' => false
            ),
            array(
                'type' => 'textarea',
                'rows' => 10,
                'cols' => 100,
                'label' => $this->l('Payment confirmation text'),
                'name' => 'payment_confirm',
                'required' => true,
                'lang' => true
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Ajax'),
                'name' => 'ajax_conf',
                'required' => true,
                'lang' => false,
                'class' => 't',
                'is_bool' => true,
                'values' => $boolean
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Skip confirmation'),
                'name' => 'skip_confirm',
                'required' => true,
                'lang' => false,
                'is_bool' => true,
                'class' => 't',
                'values' => $boolean
            )
        );
    }

    public function getStatusName()
    {
        return 'PS_OS_LIISI_BANKLINK';
    }

    public function getFormFields($order, $server)
    {
        $currency = CurrencyCore::getCurrency($order->id_currency);
        $VK = array();
        $VK['VK_SERVICE'] = $this->config[$server]['vk_service'];
        $VK['VK_VERSION'] = $this->config[$server]['vk_version'];
        $VK['VK_SND_ID'] = $this->config[$server]['vk_snd'];
        $VK['VK_STAMP'] = $order->id;
        $VK['VK_AMOUNT'] = Tools::ps_round($order->total_paid, 2);
        $VK['VK_CURR'] = $currency['iso_code'];
        if ($this->config[$server]['vk_service'] == '1011') {
            $VK['VK_ACC'] = $this->config[$server]['vk_acc'];
            $VK['VK_NAME'] = $this->config[$server]['vk_name'];
        }
        $VK['VK_REF'] = $this->config[$server]['vk_ref'];
        $VK['VK_MSG'] = $this->l('Order ') . $order->id;
        $VK['VK_RETURN'] = $this->context->link->getModuleLink($this->name, 'val', array(), true);
        $VK['VK_CANCEL'] = $VK['VK_RETURN'];

        $datetime = new DateTime();
        $VK['VK_DATETIME'] = $datetime->format(DateTime::ISO8601);

        $VK['VK_MAC'] = $this->generateMAC($VK, $server);
        $VK['VK_ENCODING'] = $this->config[$server]['vk_charset'];
        $VK['VK_LANG'] = $this->config[$server]['vk_lang'];
        return $VK;
    }

    public function getEncoding()
    {
        return 'UTF-8';
    }
}
